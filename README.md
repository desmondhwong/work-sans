'Work Sans' (former working title 'Alice')
===

A SIL Open Font project titled 'Work Sans' (former working title 'Alice') for Latin and Telugu.

Work Sans is a superfamily based loosely on early Grotesques — i.e. [Stephenson Blake](https://www.flickr.com/photos/stewf/14444337254/), [Miller & Richard](https://archive.org/stream/printingtypespec00millrich#page/226/mode/2up/) and [Bauerschen Giesserei](https://archive.org/stream/hauptprobeingedr00baue#page/109/mode/1up). There will be 9 weights across 5 widths. The core of the fonts are optimised for on-screen medium-sized text usage (14px-48px) – but still very usable in print. The fonts closer to the extreme widths and weights are designed more for display use. Overall, features are simplified and optimised for screen resolutions – for example, diacritic marks are larger than how they would be in print.

Source files have been partly prepared for use in [Metapolator](http://www.metapolator.com). 

*Note: There are no UFOs as UFOs exported from Glyphs App will currently not generate in Robofont and so on due to using '[Bracket trick](http://www.glyphsapp.com/tutorials/alternating-glyph-shapes)' and '[Brace trick](http://www.glyphsapp.com/tutorials/additional-masters-for-individual-glyphs-the-brace-trick)' to solve interpolation bumps. Glyphs marked orange are bracket layers and glyphs marked yellow are using brace layers.*

The project started in June 2014 and the brief was to have a [minimum viable product](http://en.wikipedia.org/wiki/Minimum_viable_product) (alpha release with 9 weights) by December 2014.

The Telugu glyph set comes from [Ramabhadra](http://teluguvijayam.org/fonts.html) by Silicon Andhra. Glyphs marked red in the file are those that have been manually cleaned up, the rest are automatically cleaned up with FontForge from Ramabhadra.

## [Download v1.02](https://github.com/weiweihuanghuang/Work-Sans/archive/v1.02.zip)

## Suggestion, Comments, Contributions
If you spot any errors or have any suggestions for improvements please email me at <a href="mailto:wweeiihhuuaanngg@gmail.com">wweeiihhuuaanngg@gmail.com</a>. Otherwise you can fork this project and modify it too.

## To do
- Windows rendering optimsation.
- Extend the OpenType features.

## Preview

[View a webfont specimen](http://weiweihuanghuang.github.io/Work-Sans/).

<a href="http://weiweihuanghuang.github.io/Work-Sans/">![Thin to Regular](https://github.com/weiweihuanghuang/Work-Sans/raw/master/src/Screenshots/waterfall.png)

![Screenshot of a test](https://github.com/weiweihuanghuang/Work-Sans/raw/master/src/Screenshots/preview4.png)</a>

## License

Work Sans is licensed under the SIL Open Font License v1.1 (<http://scripts.sil.org/OFL>)

To view the copyright and specific terms and conditions please refer to [OFL.txt](https://github.com/weiweihuanghuang/Work-Sans/blob/master/OFL.txt).